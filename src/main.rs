use serenity::{
    async_trait,
    client::{Client, Context, EventHandler},
    framework::standard::{
        Args,
        macros::{command, group},
        CommandResult, StandardFramework,
    },
    model::channel::Message,
    Result as SerenityResult,
};
use dotenv;

#[group]
#[commands(ping, count)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("$"))
        .group(&GENERAL_GROUP);

    dotenv::dotenv().ok();
    let token = dotenv::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn ping(context: &Context, msg: &Message) -> CommandResult {
    check_msg(msg.channel_id.say(&context.http, "Pong!").await);
    Ok(())
}

#[command]
async fn count(context: &Context, msg: &Message, args: Args) -> CommandResult {
    let system_name = &args.rest();
    if system_name.is_empty() {
        check_msg(
            msg.channel_id
                .send_message(&context.http, |m| {
                    m.content(format!("Count logic here"));
                    m
                })
                .await,
        );
    } else {
        check_msg(
            msg.channel_id
                .send_message(&context.http, |m| {
                    m.content(format!("Count logic here for system: {}", system_name.to_uppercase()));
                    m
                })
                .await,
        );
    }
    Ok(())
}

fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        println!("Error sending message: {:?}", why);
    }
}
